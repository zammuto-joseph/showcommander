﻿namespace showCommander2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelTopRight = new System.Windows.Forms.Panel();
            this.buttonGoForward = new System.Windows.Forms.Button();
            this.buttonGoBack = new System.Windows.Forms.Button();
            this.panelBottomLeft = new System.Windows.Forms.Panel();
            this.labelLater = new System.Windows.Forms.Label();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.panelLater10 = new System.Windows.Forms.Panel();
            this.panelLater9 = new System.Windows.Forms.Panel();
            this.panelLater8 = new System.Windows.Forms.Panel();
            this.panelLater7 = new System.Windows.Forms.Panel();
            this.panelLater6 = new System.Windows.Forms.Panel();
            this.panelLater5 = new System.Windows.Forms.Panel();
            this.panelLater4 = new System.Windows.Forms.Panel();
            this.panelLater3 = new System.Windows.Forms.Panel();
            this.panelLater2 = new System.Windows.Forms.Panel();
            this.panelLater1 = new System.Windows.Forms.Panel();
            this.panelBottomRight = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonGo30 = new System.Windows.Forms.Button();
            this.buttonGo25 = new System.Windows.Forms.Button();
            this.buttonGo20 = new System.Windows.Forms.Button();
            this.buttonGo15 = new System.Windows.Forms.Button();
            this.buttonGo10 = new System.Windows.Forms.Button();
            this.buttonGo5 = new System.Windows.Forms.Button();
            this.buttonTimer = new System.Windows.Forms.Button();
            this.panelTopLeft = new System.Windows.Forms.Panel();
            this.panelNextScene = new System.Windows.Forms.Panel();
            this.labelNext = new System.Windows.Forms.Label();
            this.panelCurrentScene = new System.Windows.Forms.Panel();
            this.labelNow = new System.Windows.Forms.Label();
            this.panelTopRight.SuspendLayout();
            this.panelBottomLeft.SuspendLayout();
            this.panelBottomRight.SuspendLayout();
            this.panelTopLeft.SuspendLayout();
            this.panelNextScene.SuspendLayout();
            this.panelCurrentScene.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTopRight
            // 
            this.panelTopRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTopRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panelTopRight.Controls.Add(this.buttonGoForward);
            this.panelTopRight.Controls.Add(this.buttonGoBack);
            this.panelTopRight.Location = new System.Drawing.Point(745, 19);
            this.panelTopRight.Margin = new System.Windows.Forms.Padding(10);
            this.panelTopRight.Name = "panelTopRight";
            this.panelTopRight.Size = new System.Drawing.Size(220, 130);
            this.panelTopRight.TabIndex = 0;
            // 
            // buttonGoForward
            // 
            this.buttonGoForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGoForward.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGoForward.FlatAppearance.BorderSize = 0;
            this.buttonGoForward.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGoForward.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGoForward.Location = new System.Drawing.Point(10, 70);
            this.buttonGoForward.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGoForward.Name = "buttonGoForward";
            this.buttonGoForward.Size = new System.Drawing.Size(200, 50);
            this.buttonGoForward.TabIndex = 8;
            this.buttonGoForward.Text = "GO FORWARD";
            this.buttonGoForward.UseVisualStyleBackColor = false;
            this.buttonGoForward.Click += new System.EventHandler(this.ButtonGoForward_Click);
            // 
            // buttonGoBack
            // 
            this.buttonGoBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGoBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buttonGoBack.FlatAppearance.BorderSize = 0;
            this.buttonGoBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGoBack.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGoBack.Location = new System.Drawing.Point(10, 10);
            this.buttonGoBack.Margin = new System.Windows.Forms.Padding(10);
            this.buttonGoBack.Name = "buttonGoBack";
            this.buttonGoBack.Size = new System.Drawing.Size(200, 50);
            this.buttonGoBack.TabIndex = 7;
            this.buttonGoBack.Text = "GO BACK";
            this.buttonGoBack.UseVisualStyleBackColor = false;
            // 
            // panelBottomLeft
            // 
            this.panelBottomLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBottomLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panelBottomLeft.Controls.Add(this.labelLater);
            this.panelBottomLeft.Controls.Add(this.buttonSettings);
            this.panelBottomLeft.Controls.Add(this.buttonEdit);
            this.panelBottomLeft.Controls.Add(this.panelLater10);
            this.panelBottomLeft.Controls.Add(this.panelLater9);
            this.panelBottomLeft.Controls.Add(this.panelLater8);
            this.panelBottomLeft.Controls.Add(this.panelLater7);
            this.panelBottomLeft.Controls.Add(this.panelLater6);
            this.panelBottomLeft.Controls.Add(this.panelLater5);
            this.panelBottomLeft.Controls.Add(this.panelLater4);
            this.panelBottomLeft.Controls.Add(this.panelLater3);
            this.panelBottomLeft.Controls.Add(this.panelLater2);
            this.panelBottomLeft.Controls.Add(this.panelLater1);
            this.panelBottomLeft.Location = new System.Drawing.Point(19, 159);
            this.panelBottomLeft.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelBottomLeft.Name = "panelBottomLeft";
            this.panelBottomLeft.Size = new System.Drawing.Size(716, 783);
            this.panelBottomLeft.TabIndex = 1;
            // 
            // labelLater
            // 
            this.labelLater.AutoSize = true;
            this.labelLater.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLater.Location = new System.Drawing.Point(20, 24);
            this.labelLater.Margin = new System.Windows.Forms.Padding(248, 10, 247, 10);
            this.labelLater.Name = "labelLater";
            this.labelLater.Size = new System.Drawing.Size(80, 22);
            this.labelLater.TabIndex = 2;
            this.labelLater.Text = "LATER:";
            // 
            // buttonSettings
            // 
            this.buttonSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonSettings.FlatAppearance.BorderSize = 0;
            this.buttonSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSettings.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSettings.Location = new System.Drawing.Point(10, 723);
            this.buttonSettings.Margin = new System.Windows.Forms.Padding(0, 0, 10, 10);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(200, 50);
            this.buttonSettings.TabIndex = 12;
            this.buttonSettings.Text = "SETTINGS";
            this.buttonSettings.UseVisualStyleBackColor = false;
            this.buttonSettings.Click += new System.EventHandler(this.ButtonSettings_Click);
            // 
            // buttonEdit
            // 
            this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(0)))), ((int)(((byte)(96)))));
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEdit.Location = new System.Drawing.Point(506, 723);
            this.buttonEdit.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(200, 50);
            this.buttonEdit.TabIndex = 11;
            this.buttonEdit.Text = "EDIT SHOW";
            this.buttonEdit.UseVisualStyleBackColor = false;
            // 
            // panelLater10
            // 
            this.panelLater10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater10.Location = new System.Drawing.Point(10, 610);
            this.panelLater10.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater10.Name = "panelLater10";
            this.panelLater10.Size = new System.Drawing.Size(696, 50);
            this.panelLater10.TabIndex = 10;
            // 
            // panelLater9
            // 
            this.panelLater9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater9.Location = new System.Drawing.Point(10, 550);
            this.panelLater9.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater9.Name = "panelLater9";
            this.panelLater9.Size = new System.Drawing.Size(696, 50);
            this.panelLater9.TabIndex = 9;
            // 
            // panelLater8
            // 
            this.panelLater8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater8.Location = new System.Drawing.Point(10, 490);
            this.panelLater8.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater8.Name = "panelLater8";
            this.panelLater8.Size = new System.Drawing.Size(696, 50);
            this.panelLater8.TabIndex = 8;
            // 
            // panelLater7
            // 
            this.panelLater7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater7.Location = new System.Drawing.Point(10, 430);
            this.panelLater7.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater7.Name = "panelLater7";
            this.panelLater7.Size = new System.Drawing.Size(696, 50);
            this.panelLater7.TabIndex = 7;
            // 
            // panelLater6
            // 
            this.panelLater6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater6.Location = new System.Drawing.Point(10, 370);
            this.panelLater6.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater6.Name = "panelLater6";
            this.panelLater6.Size = new System.Drawing.Size(696, 50);
            this.panelLater6.TabIndex = 6;
            // 
            // panelLater5
            // 
            this.panelLater5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater5.Location = new System.Drawing.Point(10, 310);
            this.panelLater5.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater5.Name = "panelLater5";
            this.panelLater5.Size = new System.Drawing.Size(696, 50);
            this.panelLater5.TabIndex = 5;
            // 
            // panelLater4
            // 
            this.panelLater4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater4.Location = new System.Drawing.Point(10, 250);
            this.panelLater4.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater4.Name = "panelLater4";
            this.panelLater4.Size = new System.Drawing.Size(696, 50);
            this.panelLater4.TabIndex = 4;
            // 
            // panelLater3
            // 
            this.panelLater3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater3.Location = new System.Drawing.Point(10, 190);
            this.panelLater3.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater3.Name = "panelLater3";
            this.panelLater3.Size = new System.Drawing.Size(696, 50);
            this.panelLater3.TabIndex = 3;
            // 
            // panelLater2
            // 
            this.panelLater2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater2.Location = new System.Drawing.Point(10, 130);
            this.panelLater2.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelLater2.Name = "panelLater2";
            this.panelLater2.Size = new System.Drawing.Size(696, 50);
            this.panelLater2.TabIndex = 2;
            // 
            // panelLater1
            // 
            this.panelLater1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelLater1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelLater1.Location = new System.Drawing.Point(10, 70);
            this.panelLater1.Margin = new System.Windows.Forms.Padding(10);
            this.panelLater1.Name = "panelLater1";
            this.panelLater1.Size = new System.Drawing.Size(696, 50);
            this.panelLater1.TabIndex = 2;
            // 
            // panelBottomRight
            // 
            this.panelBottomRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelBottomRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panelBottomRight.Controls.Add(this.buttonCancel);
            this.panelBottomRight.Controls.Add(this.buttonGo30);
            this.panelBottomRight.Controls.Add(this.buttonGo25);
            this.panelBottomRight.Controls.Add(this.buttonGo20);
            this.panelBottomRight.Controls.Add(this.buttonGo15);
            this.panelBottomRight.Controls.Add(this.buttonGo10);
            this.panelBottomRight.Controls.Add(this.buttonGo5);
            this.panelBottomRight.Controls.Add(this.buttonTimer);
            this.panelBottomRight.Location = new System.Drawing.Point(745, 159);
            this.panelBottomRight.Margin = new System.Windows.Forms.Padding(0, 0, 10, 10);
            this.panelBottomRight.Name = "panelBottomRight";
            this.panelBottomRight.Size = new System.Drawing.Size(220, 783);
            this.panelBottomRight.TabIndex = 2;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonCancel.FlatAppearance.BorderSize = 0;
            this.buttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(10, 723);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(200, 50);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "CANCEL";
            this.buttonCancel.UseVisualStyleBackColor = false;
            // 
            // buttonGo30
            // 
            this.buttonGo30.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo30.FlatAppearance.BorderSize = 0;
            this.buttonGo30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo30.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo30.Location = new System.Drawing.Point(10, 550);
            this.buttonGo30.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo30.Name = "buttonGo30";
            this.buttonGo30.Size = new System.Drawing.Size(200, 50);
            this.buttonGo30.TabIndex = 5;
            this.buttonGo30.Text = "GO 30";
            this.buttonGo30.UseVisualStyleBackColor = false;
            this.buttonGo30.Click += new System.EventHandler(this.ButtonGo30_Click);
            // 
            // buttonGo25
            // 
            this.buttonGo25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo25.FlatAppearance.BorderSize = 0;
            this.buttonGo25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo25.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo25.Location = new System.Drawing.Point(10, 490);
            this.buttonGo25.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo25.Name = "buttonGo25";
            this.buttonGo25.Size = new System.Drawing.Size(200, 50);
            this.buttonGo25.TabIndex = 4;
            this.buttonGo25.Text = "GO 25";
            this.buttonGo25.UseVisualStyleBackColor = false;
            this.buttonGo25.Click += new System.EventHandler(this.ButtonGo25_Click);
            // 
            // buttonGo20
            // 
            this.buttonGo20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo20.FlatAppearance.BorderSize = 0;
            this.buttonGo20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo20.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo20.Location = new System.Drawing.Point(10, 430);
            this.buttonGo20.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo20.Name = "buttonGo20";
            this.buttonGo20.Size = new System.Drawing.Size(200, 50);
            this.buttonGo20.TabIndex = 3;
            this.buttonGo20.Text = "GO 20";
            this.buttonGo20.UseVisualStyleBackColor = false;
            this.buttonGo20.Click += new System.EventHandler(this.ButtonGo20_Click);
            // 
            // buttonGo15
            // 
            this.buttonGo15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo15.FlatAppearance.BorderSize = 0;
            this.buttonGo15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo15.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo15.Location = new System.Drawing.Point(10, 370);
            this.buttonGo15.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo15.Name = "buttonGo15";
            this.buttonGo15.Size = new System.Drawing.Size(200, 50);
            this.buttonGo15.TabIndex = 2;
            this.buttonGo15.Text = "GO 15";
            this.buttonGo15.UseVisualStyleBackColor = false;
            this.buttonGo15.Click += new System.EventHandler(this.ButtonGo15_Click);
            // 
            // buttonGo10
            // 
            this.buttonGo10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo10.FlatAppearance.BorderSize = 0;
            this.buttonGo10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo10.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo10.Location = new System.Drawing.Point(10, 310);
            this.buttonGo10.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo10.Name = "buttonGo10";
            this.buttonGo10.Size = new System.Drawing.Size(200, 50);
            this.buttonGo10.TabIndex = 1;
            this.buttonGo10.Text = "GO 10";
            this.buttonGo10.UseVisualStyleBackColor = false;
            this.buttonGo10.Click += new System.EventHandler(this.ButtonGo10_Click);
            // 
            // buttonGo5
            // 
            this.buttonGo5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonGo5.FlatAppearance.BorderSize = 0;
            this.buttonGo5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGo5.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGo5.Location = new System.Drawing.Point(10, 250);
            this.buttonGo5.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.buttonGo5.Name = "buttonGo5";
            this.buttonGo5.Size = new System.Drawing.Size(200, 50);
            this.buttonGo5.TabIndex = 0;
            this.buttonGo5.Text = "GO 5";
            this.buttonGo5.UseVisualStyleBackColor = false;
            this.buttonGo5.Click += new System.EventHandler(this.ButtonGo5_Click);
            // 
            // buttonTimer
            // 
            this.buttonTimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTimer.BackColor = System.Drawing.Color.Black;
            this.buttonTimer.FlatAppearance.BorderSize = 0;
            this.buttonTimer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.buttonTimer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Black;
            this.buttonTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTimer.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTimer.Location = new System.Drawing.Point(10, 10);
            this.buttonTimer.Margin = new System.Windows.Forms.Padding(10);
            this.buttonTimer.Name = "buttonTimer";
            this.buttonTimer.Size = new System.Drawing.Size(200, 200);
            this.buttonTimer.TabIndex = 0;
            this.buttonTimer.Text = "TIME";
            this.buttonTimer.UseVisualStyleBackColor = false;
            // 
            // panelTopLeft
            // 
            this.panelTopLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelTopLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.panelTopLeft.Controls.Add(this.panelNextScene);
            this.panelTopLeft.Controls.Add(this.panelCurrentScene);
            this.panelTopLeft.Location = new System.Drawing.Point(19, 19);
            this.panelTopLeft.Margin = new System.Windows.Forms.Padding(10);
            this.panelTopLeft.Name = "panelTopLeft";
            this.panelTopLeft.Size = new System.Drawing.Size(716, 130);
            this.panelTopLeft.TabIndex = 3;
            // 
            // panelNextScene
            // 
            this.panelNextScene.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelNextScene.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(16)))), ((int)(((byte)(16)))), ((int)(((byte)(16)))));
            this.panelNextScene.Controls.Add(this.labelNext);
            this.panelNextScene.Location = new System.Drawing.Point(10, 70);
            this.panelNextScene.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelNextScene.Name = "panelNextScene";
            this.panelNextScene.Size = new System.Drawing.Size(696, 50);
            this.panelNextScene.TabIndex = 1;
            // 
            // labelNext
            // 
            this.labelNext.AutoSize = true;
            this.labelNext.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNext.Location = new System.Drawing.Point(10, 14);
            this.labelNext.Margin = new System.Windows.Forms.Padding(10, 14, 3, 14);
            this.labelNext.Name = "labelNext";
            this.labelNext.Size = new System.Drawing.Size(67, 22);
            this.labelNext.TabIndex = 1;
            this.labelNext.Text = "NEXT:";
            // 
            // panelCurrentScene
            // 
            this.panelCurrentScene.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelCurrentScene.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panelCurrentScene.Controls.Add(this.labelNow);
            this.panelCurrentScene.Location = new System.Drawing.Point(10, 10);
            this.panelCurrentScene.Margin = new System.Windows.Forms.Padding(10);
            this.panelCurrentScene.Name = "panelCurrentScene";
            this.panelCurrentScene.Size = new System.Drawing.Size(696, 50);
            this.panelCurrentScene.TabIndex = 0;
            // 
            // labelNow
            // 
            this.labelNow.AutoSize = true;
            this.labelNow.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNow.Location = new System.Drawing.Point(10, 14);
            this.labelNow.Margin = new System.Windows.Forms.Padding(10, 14, 3, 14);
            this.labelNow.Name = "labelNow";
            this.labelNow.Size = new System.Drawing.Size(62, 22);
            this.labelNow.TabIndex = 0;
            this.labelNow.Text = "NOW:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(984, 961);
            this.Controls.Add(this.panelTopLeft);
            this.Controls.Add(this.panelBottomRight);
            this.Controls.Add(this.panelBottomLeft);
            this.Controls.Add(this.panelTopRight);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "showCommander";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelTopRight.ResumeLayout(false);
            this.panelBottomLeft.ResumeLayout(false);
            this.panelBottomLeft.PerformLayout();
            this.panelBottomRight.ResumeLayout(false);
            this.panelTopLeft.ResumeLayout(false);
            this.panelNextScene.ResumeLayout(false);
            this.panelNextScene.PerformLayout();
            this.panelCurrentScene.ResumeLayout(false);
            this.panelCurrentScene.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelTopRight;
        private System.Windows.Forms.Panel panelBottomLeft;
        private System.Windows.Forms.Panel panelBottomRight;
        public System.Windows.Forms.Button buttonGo5;
        public System.Windows.Forms.Button buttonGo30;
        public System.Windows.Forms.Button buttonGo25;
        public System.Windows.Forms.Button buttonGo20;
        public System.Windows.Forms.Button buttonGo15;
        public System.Windows.Forms.Button buttonGo10;
        private System.Windows.Forms.Button buttonCancel;
        public System.Windows.Forms.Button buttonGoForward;
        private System.Windows.Forms.Button buttonGoBack;
        private System.Windows.Forms.Panel panelTopLeft;
        private System.Windows.Forms.Panel panelNextScene;
        private System.Windows.Forms.Panel panelCurrentScene;
        private System.Windows.Forms.Panel panelLater4;
        private System.Windows.Forms.Panel panelLater3;
        private System.Windows.Forms.Panel panelLater2;
        private System.Windows.Forms.Panel panelLater1;
        private System.Windows.Forms.Panel panelLater10;
        private System.Windows.Forms.Panel panelLater9;
        private System.Windows.Forms.Panel panelLater8;
        private System.Windows.Forms.Panel panelLater7;
        private System.Windows.Forms.Panel panelLater6;
        private System.Windows.Forms.Panel panelLater5;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Label labelLater;
        private System.Windows.Forms.Label labelNext;
        private System.Windows.Forms.Label labelNow;
        public System.Windows.Forms.Button buttonTimer;
    }
}

