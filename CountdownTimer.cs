﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;

namespace showCommander2
{
    class CountdownTimer
    {
        public CountdownTimer() { }
        public void startCountdown(Button buttonTimer, int time)
        {
            if (buttonTimer.InvokeRequired)
            {
                buttonTimer.Invoke((Action)delegate ()
                {
                    startCountdown(buttonTimer, time);
                });
            }
            else
            {
                for (int i = time; i > 0; i--)
                {
                    if (i > 10)
                    {
                        buttonTimer.BackColor = Color.FromArgb(64, 64, 64);
                        buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(64, 64, 64);
                        buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(64, 64, 64);
                    }
                    else if (i > 5 && i < 11)
                    {
                        buttonTimer.BackColor = Color.FromArgb(192, 128, 0);
                        buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 128, 0);
                        buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 128, 0);
                    }
                    else
                    {
                        buttonTimer.BackColor = Color.FromArgb(192, 0, 0);
                        buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 0, 0);
                        buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 0, 0);
                    }
                    buttonTimer.Text = i.ToString();
                    Application.DoEvents();
                    Thread.Sleep(1000);
                }
                buttonTimer.Text = "GO";
                buttonTimer.BackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 192, 0);
                Application.DoEvents();
                Thread.Sleep(250);
                buttonTimer.BackColor = Color.FromArgb(192, 0, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 0, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 0, 0);
                Application.DoEvents();
                Thread.Sleep(250);
                buttonTimer.BackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 192, 0);
                Application.DoEvents();
                Thread.Sleep(250);
                buttonTimer.BackColor = Color.FromArgb(192, 0, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(192, 0, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(192, 0, 0);
                Application.DoEvents();
                Thread.Sleep(250);
                buttonTimer.BackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 192, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 192, 0);
                Application.DoEvents();
                Thread.Sleep(250);
                buttonTimer.Text = "TIME";
                buttonTimer.BackColor = Color.FromArgb(0, 0, 0);
                buttonTimer.FlatAppearance.MouseOverBackColor = Color.FromArgb(0, 0, 0);
                buttonTimer.FlatAppearance.MouseDownBackColor = Color.FromArgb(0, 0, 0);
            }
        }
    }
}
