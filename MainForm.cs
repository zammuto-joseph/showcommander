﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace showCommander2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public int time;
        Thread countdownThread;

        private void ButtonGo5_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 5;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void createCountdownTimer()
        {
            CountdownTimer countdownTimer = new CountdownTimer();
            countdownTimer.startCountdown(this.buttonTimer, time);
            enableButtons();
        }

        private void ButtonGoForward_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 0;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void ButtonGo10_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 10;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void ButtonGo15_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 15;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void ButtonGo20_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 20;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void ButtonGo25_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 25;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }

        private void ButtonGo30_Click(object sender, EventArgs e)
        {
            disableButtons();
            time = 30;
            countdownThread = new Thread(new ThreadStart(createCountdownTimer));
            countdownThread.Start();
        }
        
        private void disableButtons()
        {
            buttonGoForward.Enabled = false;
            buttonGo5.Enabled = false;
            buttonGo10.Enabled = false;
            buttonGo15.Enabled = false;
            buttonGo20.Enabled = false;
            buttonGo25.Enabled = false;
            buttonGo30.Enabled = false;
            buttonGoBack.Enabled = false;
        }

        public void enableButtons()
        {
            if (buttonGoForward.InvokeRequired)
            {
                buttonGoForward.Invoke((Action)delegate ()
                {
                    enableButtons();
                });
            }
            buttonGoForward.Enabled = true;
            buttonGo5.Enabled = true;
            buttonGo10.Enabled = true;
            buttonGo15.Enabled = true;
            buttonGo20.Enabled = true;
            buttonGo25.Enabled = true;
            buttonGo30.Enabled = true;
            buttonGoBack.Enabled = true;
        }

        private void ButtonSettings_Click(object sender, EventArgs e)
        {
            Thread settingsThrad = new Thread(new ThreadStart(displaySettingsForm));
            settingsThrad.Start();
        }

        public void displaySettingsForm()
        {
            SettingsForm settingsForm = new SettingsForm();
            Application.Run(settingsForm);
        }
    }
}
